-- DEFAULT CHARSET=utf8mb4
-- COLLATE=utf8mb4_general_ci;

-- RECONSTRUCTION DE LA BASE DE DONNEE

DROP DATABASE IF EXISTS webStore;
CREATE DATABASE webStore;
USE webStore;

-- CONSTRUCTION DE LA TABLE DES UTILISATEURS

CREATE TABLE T_Users (
	IdUser				int(4) PRIMARY KEY AUTO_INCREMENT,
	Login				varchar(20) NOT NULL,
	Password			varchar(20) NOT NULL,
	ConnectionNumber	int(4) NOT NULL DEFAULT (0)
) ENGINE = InnoDB;

INSERT INTO T_Users (IdUser, Login, Password) VALUES (1, 'Jack', 'Sparrow');
INSERT INTO T_Users (IdUser, Login, Password) VALUES (2, 'Skywalker', 'Luck');
INSERT INTO T_Users (IdUser, Login, Password) VALUES (3, 'Plissken', 'Snake');
INSERT INTO T_Users (IdUser, Login, Password) VALUES (4, 'Anderson', 'Neo');
INSERT INTO T_Users (IdUser, Login, Password) VALUES (5, 'Leonhart', 'Squall');

SELECT * FROM T_Users;

-- ------------------------------------------
